﻿using AlterDataVotador.Domain.ViewModel;
using AlterDataVotador.Domain.ViewModel.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlterdataVotador.Application.Admin.Interfaces
{
    public interface ISetorAppService
    {
        ServiceResult<List<SetorResult>> ListSetor(SetorListParameter paramObj);
        ServiceResult<SetorResult> InsertSetor(SetorInsertParameter paramObj);
        ServiceResult<Boolean> UpdateSetor(SetorUpdateParameter paramObj);
        ServiceResult<Boolean> DeleteSetor(SetorDeleteParameter paramObj);
    }
}
