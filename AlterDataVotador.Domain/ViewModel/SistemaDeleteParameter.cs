﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlterDataVotador.Domain.ViewModel
{
    public class SistemaDeleteParameter
    {
        /// <summary>
        /// Id do Sistema
        /// </summary>
        public Int32 IdSistema { get; set; }
    }
}
