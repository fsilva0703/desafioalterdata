﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlterDataVotador.Domain.ViewModel
{
    public class SistemaListParameter
    {
        /// <summary>
        /// Nome do Sistema
        /// </summary>
        public String NomeSistema { get; set; }
    }
}
