﻿using AlterDataVotador.Domain.ViewModel;
using AlterDataVotador.Domain.ViewModel.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlterDataVotador.Domain.Admin.Interfaces
{
    public interface ISetorService
    {
        ServiceResult<List<SetorResult>> ListSetor(SetorListParameter paramObj);
        ServiceResult<SetorResult> InsertSetor(SetorInsertParameter paramObj);
        ServiceResult<Boolean> UpdateSetor(SetorUpdateParameter paramObj);
        ServiceResult<Boolean> DeleteSetor(SetorDeleteParameter paramObj);
    }
}
