﻿using AlterDataVotador.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlterDataVotador.Domain.Admin.Interfaces.Repositories
{
    public interface ISetorRepository
    {
        List<SetorResult> ListSetor(SetorListParameter paramObj);
        SetorResult InsertSetor(SetorInsertParameter paramObj);
        Boolean UpdateSetor(SetorUpdateParameter paramObj);
        Boolean DeleteSetor(SetorDeleteParameter paramObj);
    }
}
