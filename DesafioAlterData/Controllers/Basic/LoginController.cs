﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlterdataVotador.Application.Admin.Interfaces;
using AlterDataVotador.Domain.ViewModel.Dto;
using AlterDataVotador.Infra.Data.Admin.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AlterDataVotador.Controllers.Basic
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("Basic/[controller]")]
    public class LoginController : Controller
    {
        private readonly ILoginAppService _loginAppService;
        public LoginController(ILoginAppService paramLoginAppService)
        {
            _loginAppService = paramLoginAppService;
        }

        [HttpGet("Login")]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody]UserInfo login)
        {
            try
            {
                var user = _loginAppService.Get(login.Email, login.Password).FirstOrDefault();

                if (user == null)
                    return BadRequest(new { message = "Usuário ou senha inválidos" });

                var token = _loginAppService.GenerateToken(user);
                user.Password = "";

                return new
                {
                    user = user,
                    token = token
                };
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}