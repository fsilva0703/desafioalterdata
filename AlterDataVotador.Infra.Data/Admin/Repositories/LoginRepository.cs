﻿using AlterDataVotador.Domain.Admin.Interfaces.Repositories;
using AlterDataVotador.Domain.ViewModel.Dto;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace AlterDataVotador.Infra.Data.Admin.Repositories
{
    public class LoginRepository : ILoginRepository
    {
        private readonly IConfiguration _configuration;

        public LoginRepository(IConfiguration config)
        {
            _configuration = config;
        }
        public string GetConnectionString()
        {
            string conString = _configuration.GetConnectionString("AlterDataVotador");
            return conString;
        }
        public List<UserInfo> Get(String email, String senha)
        {
            try
            {
                List<UserInfo> lst = null;

                using (SqlConnection con = new SqlConnection(GetConnectionString()))
                {
                    SqlCommand cmd = new SqlCommand("Autentica_User", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataReader sqlDataReader = null;

                    cmd.Parameters.AddWithValue("@paramEmail", email);
                    cmd.Parameters.AddWithValue("@paramSenha", senha);

                    con.Open();
                    sqlDataReader = cmd.ExecuteReader();

                    lst = new List<UserInfo>();
                    while (sqlDataReader.Read())
                    {
                        UserInfo item = new UserInfo();
                        if (!sqlDataReader.IsDBNull(0)) item.Id = sqlDataReader.GetInt32(0);
                        if (!sqlDataReader.IsDBNull(1)) item.Email = sqlDataReader.GetString(1);
                        if (!sqlDataReader.IsDBNull(2)) item.Password = sqlDataReader.GetString(2);
                        if (!sqlDataReader.IsDBNull(3)) item.Setor = sqlDataReader.GetString(3);

                        lst.Add(item);
                    }

                    con.Close();
                }

                return lst;

            }
            catch (Exception ex)
            {
                throw new Exception("Ops... Ocorreu um erro na listagem dos recursos: " + ex.Message);
            }
        }
    }
}
